# translation of kcmkeyboard.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ravishankar Shrivastava <raviratlami@yahoo.com>, 2008.
# Ravishankar Shrivastava <raviratlami@aol.in>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2009-01-23 15:19+0530\n"
"Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>\n"
"Language-Team: Hindi <kde-i18n-doc@lists.kde.org>\n"
"Language: hne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.2\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "रविसंकर सिरीवास्तव, जी. करूनाकर"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raviratlami@aol.in"

#: bindings.cpp:24
#, fuzzy, kde-format
#| msgid "KDE Keyboard Layout Switcher"
msgid "Keyboard Layout Switcher"
msgstr "केडीई कुंजीपटल खाका बदलइया"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "अगले कुंजीपट खाका मं स्विच करव"

#: bindings.cpp:30
#, fuzzy, kde-format
#| msgid "Switch to Next Keyboard Layout"
msgid "Switch to Last-Used Keyboard Layout"
msgstr "अगले कुंजीपट खाका मं स्विच करव"

#: bindings.cpp:60
#, fuzzy, kde-format
#| msgid "Error changing keyboard layout to '%1'"
msgid "Switch keyboard layout to %1"
msgstr "कुंजीपट खाका ल '%1' मं बदले मं गलती होइस"

#: keyboardmodel.cpp:35
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr ""

#: keyboardmodel.cpp:39
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr ""

#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "कुंजी के दोहराव"

#: tastenbrett/main.cpp:54
#, fuzzy, kde-format
#| msgid "KDE Keyboard Layout Switcher"
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "केडीई कुंजीपटल खाका बदलइया"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant.\n"
"Previewing layouts that are defined outside your systems xkb directory is "
"not supported and  will also trigger this message. These might still work "
"fine if applied"
msgstr ""

#: ui/Advanced.qml:40
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgctxt "@option:checkbox"
msgid "Configure keyboard options"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#: ui/Hardware.qml:34
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "@title:group"
msgid "Keyboard model:"
msgstr "कुंजी के दोहराव"

#: ui/Hardware.qml:68
#, fuzzy, kde-format
#| msgid "NumLock on KDE Startup"
msgctxt "@title:group"
msgid "NumLock on Plasma Startup:"
msgstr "केडीई चालू होय के बेरा मं न्यूम-लाक"

#: ui/Hardware.qml:73
#, fuzzy, kde-format
#| msgid "T&urn on"
msgctxt "@option:radio"
msgid "Turn On"
msgstr "चालू करव (&u)"

#: ui/Hardware.qml:77
#, fuzzy, kde-format
#| msgid "Turn o&ff"
msgctxt "@option:radio"
msgid "Turn Off"
msgstr "बन्द करव (&f)"

#: ui/Hardware.qml:81
#, fuzzy, kde-format
#| msgid "Leave unchan&ged"
msgctxt "@option:radio"
msgid "Leave unchanged"
msgstr "बिन बदले छोड़ देव (&g)"

#: ui/Hardware.qml:114
#, kde-format
msgctxt "@title:group"
msgid "When key is held:"
msgstr ""

#: ui/Hardware.qml:119
#, kde-format
msgctxt "@option:radio"
msgid "Repeat the key"
msgstr ""

#: ui/Hardware.qml:124
#, kde-format
msgctxt "@option:radio"
msgid "Do nothing"
msgstr ""

#: ui/Hardware.qml:129
#, kde-format
msgctxt "@option:radio"
msgid "Show accented and similar characters"
msgstr ""

#: ui/Hardware.qml:164
#, fuzzy, kde-format
#| msgid "&Delay:"
msgctxt "@label:slider"
msgid "Delay:"
msgstr "देरीः (&D)"

#: ui/Hardware.qml:199
#, fuzzy, kde-format
#| msgid "&Rate:"
msgctxt "@label:slider"
msgid "Rate:"
msgstr "दरः (&R)"

#: ui/Hardware.qml:247
#, kde-format
msgctxt "@label:textbox"
msgid "Test area:"
msgstr ""

#: ui/Hardware.qml:248
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to test settings"
msgstr ""

#: ui/LayoutDialog.qml:21
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "@title:window"
msgid "Add Layout"
msgstr "कुंजी के दोहराव"

#: ui/LayoutDialog.qml:106 ui/Layouts.qml:245
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "@action:button"
msgid "Preview"
msgstr "कुंजी के दोहराव"

#: ui/LayoutDialog.qml:124
#, kde-format
msgctxt "@label:textbox"
msgid "Display text:"
msgstr ""

#: ui/LayoutDialog.qml:143
#, fuzzy, kde-format
#| msgid "Main shortcuts:"
msgctxt "@option:textbox"
msgid "Shortcut:"
msgstr "मुख्य सार्टकट:"

#: ui/Layouts.qml:28
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgctxt "@title:group"
msgid "Shortcuts for Switching Layout"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#: ui/Layouts.qml:34
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgctxt "@option:textbox"
msgid "Main shortcuts:"
msgstr "वैकल्पिक सार्टकट:"

#: ui/Layouts.qml:61
#, fuzzy, kde-format
#| msgid "3rd level shortcuts:"
msgctxt "@option:textbox"
msgid "3rd level shortcuts:"
msgstr "३रा स्तर सार्टकट:"

#: ui/Layouts.qml:88
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgctxt "@option:textbox"
msgid "Alternative shortcut:"
msgstr "वैकल्पिक सार्टकट:"

#: ui/Layouts.qml:109
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgctxt "@option:textbox"
msgid "Last used shortcuts:"
msgstr "वैकल्पिक सार्टकट:"

#: ui/Layouts.qml:129
#, kde-format
msgctxt "@option:checkbox"
msgid "Show a popup on layout changes"
msgstr ""

#: ui/Layouts.qml:145
#, fuzzy, kde-format
#| msgid "Switching Policy"
msgctxt "@title:group"
msgid "Switching Policy"
msgstr "स्विचिंग नीति"

#: ui/Layouts.qml:152
#, fuzzy, kde-format
#| msgid "&Global"
msgctxt "@option:radio"
msgid "Global"
msgstr "वैस्विक (&G)"

#: ui/Layouts.qml:156
#, fuzzy, kde-format
#| msgid "&Desktop"
msgctxt "@option:radio"
msgid "Desktop"
msgstr "डेस्कटाप (&D)"

#: ui/Layouts.qml:160
#, fuzzy, kde-format
#| msgid "&Application"
msgctxt "@option:radio"
msgid "Application"
msgstr "अनुपरयोग (&A)"

#: ui/Layouts.qml:164
#, fuzzy, kde-format
#| msgid "&Window"
msgctxt "@option:radio"
msgid "Window"
msgstr "विंडो (&W)"

#: ui/Layouts.qml:185
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgctxt "@option:checkbox"
msgid "Configure Layouts"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#: ui/Layouts.qml:200
#, kde-format
msgctxt "@action:button"
msgid "Add"
msgstr ""

#: ui/Layouts.qml:206
#, kde-format
msgctxt "@action:button"
msgid "Remove"
msgstr ""

#: ui/Layouts.qml:213
#, kde-format
msgctxt "@action:button"
msgid "Move Up"
msgstr ""

#: ui/Layouts.qml:229
#, kde-format
msgctxt "@action:button"
msgid "Move Down"
msgstr ""

#: ui/Layouts.qml:316
#, fuzzy, kde-format
#| msgid "Map"
msgctxt "@title:column"
msgid "Map"
msgstr "मैप"

#: ui/Layouts.qml:323
#, fuzzy, kde-format
msgctxt "@title:column"
msgid "Label"
msgstr "लेबल:"

#: ui/Layouts.qml:332
#, fuzzy, kde-format
#| msgid "Layout"
msgctxt "@title:column"
msgid "Layout"
msgstr "खाका"

#: ui/Layouts.qml:337
#, fuzzy, kde-format
#| msgid "Variant"
msgctxt "@title:column"
msgid "Variant"
msgstr "वेरियंट"

#: ui/Layouts.qml:342
#, fuzzy, kde-format
#| msgid "Main shortcuts:"
msgctxt "@title:column"
msgid "Shortcut"
msgstr "मुख्य सार्टकट:"

#: ui/Layouts.qml:388
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgctxt "@action:button"
msgid "Reassign shortcut"
msgstr "वैकल्पिक सार्टकट:"

#: ui/Layouts.qml:404
#, kde-format
msgctxt "@action:button"
msgid "Cancel assignment"
msgstr ""

#: ui/Layouts.qml:417
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgctxt "@option:checkbox"
msgid "Spare layouts"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#: ui/Layouts.qml:453
#, kde-format
msgctxt "@label:spinbox"
msgid "Main layout count:"
msgstr ""

#: ui/main.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Hardware"
msgstr ""

#: ui/main.qml:49
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgctxt "@title:tab"
msgid "Layouts"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#: ui/main.qml:55
#, kde-format
msgctxt "@title:tab"
msgid "Key Bindings"
msgstr ""

#: xkboptionsmodel.cpp:160
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] ""
msgstr[1] ""

#: xkboptionsmodel.cpp:163
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no shortcuts defined"
msgid "None"
msgstr "कुछ नइ"

#~ msgid "Label:"
#~ msgstr "लेबल:"

#~ msgid ""
#~ "If you select \"Application\" or \"Window\" switching policy, changing "
#~ "the keyboard layout will only affect the current application or window."
#~ msgstr ""
#~ "यदि आप मन चुनथव \"अनुपरयोग\" या \"विंडो\" स्विचिंग नीति,  कुंजीपट खाका बदले जाना "
#~ "सिरिफ अभी हाल के अनुपरयोग या विंडो ल प्रभावित करही."

#~ msgid ""
#~ "This is a shortcut for switching layouts which is handled by X.org. It "
#~ "allows modifier-only shortcuts."
#~ msgstr ""
#~ "ए ह खाका ल स्विच करे के सार्टकट हे, जऊन ल X.org हेंडल करथे. ए सिरिफ-माडीफायर "
#~ "सार्टकट ल स्वीकारथे."

#~ msgid "Advanced"
#~ msgstr "विस्तृत"

#, fuzzy
#~| msgctxt "Default variant"
#~| msgid "Default"
#~ msgctxt "variant"
#~ msgid "Default"
#~ msgstr "डिफाल्ट"

#~ msgid "Leave unchan&ged"
#~ msgstr "बिन बदले छोड़ देव (&g)"

#, fuzzy
#~| msgid ""
#~| "If supported, this option allows you to set the rate at which keycodes "
#~| "are generated while a key is pressed."
#~ msgid ""
#~ "If supported, this option allows you to set the delay after which a "
#~ "pressed key will start generating keycodes. The 'Repeat rate' option "
#~ "controls the frequency of these keycodes."
#~ msgstr ""
#~ "यदि समर्थित होही, तहां ए विकल्प ह आप मन ल जब कुंजी पट दबाय जाही, तहां कऊन दर से "
#~ "की-कोड जेनरेट होही, एला सेट करन देथे ."

#~ msgid ""
#~ "If supported, this option allows you to set the rate at which keycodes "
#~ "are generated while a key is pressed."
#~ msgstr ""
#~ "यदि समर्थित होही, तहां ए विकल्प ह आप मन ल जब कुंजी पट दबाय जाही, तहां कऊन दर से "
#~ "की-कोड जेनरेट होही, एला सेट करन देथे ."

#, fuzzy
#~| msgid "KDE Keyboard Layout Switcher"
#~ msgid "KDE Keyboard Control Module"
#~ msgstr "केडीई कुंजीपटल खाका बदलइया"

#, fuzzy
#~| msgid "Copyright (C) 2006-2007 Andriy Rysin"
#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "कापीराइट (c) 2006-2007 आन्द्रेई राइसिन"

#, fuzzy
#~| msgid "KDE Keyboard Layout Switcher"
#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "केडीई कुंजीपटल खाका बदलइया"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Show for single layout"
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#, fuzzy
#~| msgid "Keyboard Repeat"
#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "कुंजी के दोहराव"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Configure Layouts..."
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#~ msgid "Keyboard Repeat"
#~ msgstr "कुंजी के दोहराव"

#~ msgid "Turn o&ff"
#~ msgstr "बन्द करव (&f)"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Configure..."
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#, fuzzy
#~| msgid "Key click &volume:"
#~ msgid "&Key click volume:"
#~ msgstr "कुंजी किलिक अवाजः (&v)"

#~ msgid "&Enable keyboard repeat"
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"
