# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Tamas Szanto <tszanto@interware.hu>, 2007.
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2011, 2014, 2015.
# Balázs Úr <urbalazs@gmail.com>, 2012.
# SPDX-FileCopyrightText: 2017, 2020, 2021, 2023 Kiszel Kristóf <kiszel.kristof@gmail.com>
# SPDX-FileCopyrightText: 2024 Kristof Kiszel <ulysses@fsf.hu>
msgid ""
msgstr ""
"Project-Id-Version: KDE 4.1\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2024-05-11 22:48+0200\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.02.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ulysses@fsf.hu"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr "Billentyűzetkiosztás-váltó"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Váltás a következő billentyűzetkiosztásra"

#: bindings.cpp:30
#, kde-format
msgid "Switch to Last-Used Keyboard Layout"
msgstr "Váltás a legutóbb használt billentyűzetkiosztásra"

#: bindings.cpp:60
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Billentyűzetkiosztást váltása erre: %1"

#: keyboardmodel.cpp:35
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Ismeretlen"

#: keyboardmodel.cpp:39
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: tastenbrett/main.cpp:52
#, kde-format
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Billentyűzet-előnézet"

#: tastenbrett/main.cpp:54
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Billentyűzetkiosztás-megjelenítő"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant.\n"
"Previewing layouts that are defined outside your systems xkb directory is "
"not supported and  will also trigger this message. These might still work "
"fine if applied"
msgstr ""
"A billentyűzetgeometria betöltése meghiúsult. Ez általában azt jelenti, hogy "
"a kiválasztott modell nem támogat egy adott kiosztást vagy kiosztás-"
"variációt. A probléma valószínűleg akkor is jelen lesz, ha egyszerre "
"próbálja meg használni ezt a modellt, kiosztást és variációt.\n"
"A rendszer xkb könyvtárán kívül definiált kiosztások előnézetének "
"megtekintése nem támogatott, és szintén ezt az üzenetet váltja ki. Ezek még "
"rendben működhetnek alkalmazva"

#: ui/Advanced.qml:40
#, kde-format
msgctxt "@option:checkbox"
msgid "Configure keyboard options"
msgstr "Billentyűopciók beállítása"

#: ui/Hardware.qml:34
#, kde-format
msgctxt "@title:group"
msgid "Keyboard model:"
msgstr "Billentyűzetmodell:"

#: ui/Hardware.qml:68
#, kde-format
msgctxt "@title:group"
msgid "NumLock on Plasma Startup:"
msgstr "A NumLock állapota induláskor:"

#: ui/Hardware.qml:73
#, kde-format
msgctxt "@option:radio"
msgid "Turn On"
msgstr "Be"

#: ui/Hardware.qml:77
#, kde-format
msgctxt "@option:radio"
msgid "Turn Off"
msgstr "Ki"

#: ui/Hardware.qml:81
#, kde-format
msgctxt "@option:radio"
msgid "Leave unchanged"
msgstr "Ne változzon"

#: ui/Hardware.qml:114
#, kde-format
msgctxt "@title:group"
msgid "When key is held:"
msgstr "Ha egy billentyűt lenyomtak:"

#: ui/Hardware.qml:119
#, kde-format
msgctxt "@option:radio"
msgid "Repeat the key"
msgstr "Billentyű ismétlése"

#: ui/Hardware.qml:124
#, kde-format
msgctxt "@option:radio"
msgid "Do nothing"
msgstr "Ne tegyen semmit"

#: ui/Hardware.qml:129
#, kde-format
msgctxt "@option:radio"
msgid "Show accented and similar characters"
msgstr "Ékezetes és hasonló karakterek megjelenítése"

#: ui/Hardware.qml:164
#, kde-format
msgctxt "@label:slider"
msgid "Delay:"
msgstr "Késleltetés:"

#: ui/Hardware.qml:199
#, kde-format
msgctxt "@label:slider"
msgid "Rate:"
msgstr "Ismétlődési sebesség:"

#: ui/Hardware.qml:247
#, kde-format
msgctxt "@label:textbox"
msgid "Test area:"
msgstr "Tesztterület:"

#: ui/Hardware.qml:248
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to test settings"
msgstr "Gépeljen itt a beállítások teszteléséhez"

#: ui/LayoutDialog.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Add Layout"
msgstr "Kiosztás hozzáadása"

#: ui/LayoutDialog.qml:106 ui/Layouts.qml:245
#, kde-format
msgctxt "@action:button"
msgid "Preview"
msgstr "Előnézet"

#: ui/LayoutDialog.qml:124
#, kde-format
msgctxt "@label:textbox"
msgid "Display text:"
msgstr "Szöveg megjelenítése:"

#: ui/LayoutDialog.qml:143
#, kde-format
msgctxt "@option:textbox"
msgid "Shortcut:"
msgstr "Billentyűparancs:"

#: ui/Layouts.qml:28
#, kde-format
msgctxt "@title:group"
msgid "Shortcuts for Switching Layout"
msgstr "Gyorsbillentyű a kiosztásváltáshoz"

#: ui/Layouts.qml:34
#, kde-format
msgctxt "@option:textbox"
msgid "Main shortcuts:"
msgstr "Fő gyorsbillentyűk:"

#: ui/Layouts.qml:61
#, kde-format
msgctxt "@option:textbox"
msgid "3rd level shortcuts:"
msgstr "3. szintű gyorsbillentyűk:"

#: ui/Layouts.qml:88
#, kde-format
msgctxt "@option:textbox"
msgid "Alternative shortcut:"
msgstr "Alternatív gyorsbillentyű:"

#: ui/Layouts.qml:109
#, kde-format
msgctxt "@option:textbox"
msgid "Last used shortcuts:"
msgstr "Legutóbbi gyorsbillentyű:"

#: ui/Layouts.qml:129
#, kde-format
msgctxt "@option:checkbox"
msgid "Show a popup on layout changes"
msgstr "Felugró megjelenítése kiosztásváltozáskor"

#: ui/Layouts.qml:145
#, kde-format
msgctxt "@title:group"
msgid "Switching Policy"
msgstr "Váltási szabály"

#: ui/Layouts.qml:152
#, kde-format
msgctxt "@option:radio"
msgid "Global"
msgstr "Globális"

#: ui/Layouts.qml:156
#, kde-format
msgctxt "@option:radio"
msgid "Desktop"
msgstr "Asztal"

#: ui/Layouts.qml:160
#, kde-format
msgctxt "@option:radio"
msgid "Application"
msgstr "Alkalmazás"

#: ui/Layouts.qml:164
#, kde-format
msgctxt "@option:radio"
msgid "Window"
msgstr "Ablak"

#: ui/Layouts.qml:185
#, kde-format
msgctxt "@option:checkbox"
msgid "Configure Layouts"
msgstr "Kiosztások beállítása"

#: ui/Layouts.qml:200
#, kde-format
msgctxt "@action:button"
msgid "Add"
msgstr "Hozzáadás"

#: ui/Layouts.qml:206
#, kde-format
msgctxt "@action:button"
msgid "Remove"
msgstr "Eltávolítás"

#: ui/Layouts.qml:213
#, kde-format
msgctxt "@action:button"
msgid "Move Up"
msgstr "Fel"

#: ui/Layouts.qml:229
#, kde-format
msgctxt "@action:button"
msgid "Move Down"
msgstr "Le"

#: ui/Layouts.qml:316
#, kde-format
msgctxt "@title:column"
msgid "Map"
msgstr "Kód"

#: ui/Layouts.qml:323
#, kde-format
msgctxt "@title:column"
msgid "Label"
msgstr "Címke"

#: ui/Layouts.qml:332
#, kde-format
msgctxt "@title:column"
msgid "Layout"
msgstr "Kiosztás"

#: ui/Layouts.qml:337
#, kde-format
msgctxt "@title:column"
msgid "Variant"
msgstr "Variáns"

#: ui/Layouts.qml:342
#, kde-format
msgctxt "@title:column"
msgid "Shortcut"
msgstr "Gyorsbillentyű"

#: ui/Layouts.qml:388
#, kde-format
msgctxt "@action:button"
msgid "Reassign shortcut"
msgstr "Gyorsbillentyűk újra kiosztása"

#: ui/Layouts.qml:404
#, kde-format
msgctxt "@action:button"
msgid "Cancel assignment"
msgstr "Hozzárendelés törlése"

#: ui/Layouts.qml:417
#, kde-format
msgctxt "@option:checkbox"
msgid "Spare layouts"
msgstr "Tartalék kiosztások"

#: ui/Layouts.qml:453
#, kde-format
msgctxt "@label:spinbox"
msgid "Main layout count:"
msgstr "Fő elrendezés-szám:"

#: ui/main.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Hardware"
msgstr "Hardver"

#: ui/main.qml:49
#, kde-format
msgctxt "@title:tab"
msgid "Layouts"
msgstr "Kiosztások"

#: ui/main.qml:55
#, kde-format
msgctxt "@title:tab"
msgid "Key Bindings"
msgstr "Billentyűparancsok"

#: xkboptionsmodel.cpp:160
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 gyorsbillentyű"
msgstr[1] "%1 gyorsbillentyű"

#: xkboptionsmodel.cpp:163
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Nincs"

#~ msgctxt "layout - variant"
#~ msgid "%1 - %2"
#~ msgstr "%1 - %2"

#~ msgid "Search…"
#~ msgstr "Keresés…"

#~ msgid "Label:"
#~ msgstr "Címke:"

#~ msgid ""
#~ "Here you can choose a keyboard model. This setting is independent of your "
#~ "keyboard layout and refers to the \"hardware\" model, i.e. the way your "
#~ "keyboard is manufactured. Modern keyboards that come with your computer "
#~ "usually have two extra keys and are referred to as \"104-key\" models, "
#~ "which is probably what you want if you do not know what kind of keyboard "
#~ "you have.\n"
#~ msgstr ""
#~ "Itt választhatja ki a billentyűzet modellt. Ez a beállítás független a "
#~ "billentyűzetkiosztásától és utal a „hardver” modellre, azaz ahogyan a "
#~ "billentyűzetet gyártották. A számítógéphez adott modern billentyűzeteknek "
#~ "általában van két extra gombja és „104 gombos” modellként utalnak rá, "
#~ "amely valószínűleg az, amelyet szeretne, ha nem tudja, hogy milyen "
#~ "fajtájú billentyűzete van.\n"

#~ msgid ""
#~ "If you select \"Application\" or \"Window\" switching policy, changing "
#~ "the keyboard layout will only affect the current application or window."
#~ msgstr ""
#~ "Ha az „Alkalmazás” vagy „Ablak” váltás szabályt választja, a "
#~ "billentyűzetkiosztás megváltoztatása csak a jelenlegi alkalmazásra vagy "
#~ "ablakra lesz hatással."

#~ msgid ""
#~ "This is a shortcut for switching layouts which is handled by X.org. It "
#~ "allows modifier-only shortcuts."
#~ msgstr ""
#~ "Ez egy gyorsbillentyű az X.org által kezelt kiosztások váltásához. Csak "
#~ "módosító gyorsbillentyűket engedélyez."

#~ msgctxt "no shortcut defined"
#~ msgid "None"
#~ msgstr "Nincs"

#~ msgid "…"
#~ msgstr "…"

#~ msgid ""
#~ "This is a shortcut for switching to a third level of the active layout "
#~ "(if it has one) which is handled by X.org. It allows modifier-only "
#~ "shortcuts."
#~ msgstr ""
#~ "Ez egy gyorsbillentyű az X.org által kezelt aktív kiosztás harmadik "
#~ "szintre váltásához (ha van ilyen). Csak módosító gyorsbillentyűket "
#~ "engedélyez."

#~ msgid ""
#~ "This is a shortcut for switching layouts. It does not support modifier-"
#~ "only shortcuts and also may not work in some situations (e.g. if popup is "
#~ "active or from screensaver)."
#~ msgstr ""
#~ "Ez egy gyorsbillentyű a kiosztások váltásához. Nem támogatja a csak "
#~ "módosító gyorsbillentyűket és nem működik bizonyos helyzetekben (például "
#~ "ha a felugrás aktív vagy a képernyővédőből)."

#~ msgid ""
#~ "This shortcut allows for fast switching between two layouts, by always "
#~ "switching to the last-used one."
#~ msgstr ""
#~ "Ez a gyorsbillentyű lehetővé teszi a gyors váltást két elrendezés között "
#~ "azzal, hogy mindig a legutóbb használtra vált."

#~ msgid "Advanced"
#~ msgstr "Speciális"

#~ msgctxt "variant"
#~ msgid "Default"
#~ msgstr "Alapértelmezett"

#~ msgid ""
#~ "Allows to test keyboard repeat and click volume (just don't forget to "
#~ "apply the changes)."
#~ msgstr ""
#~ "Lehetővé teszi a billentyűzet ismétlésének és a kattintás hangerejének "
#~ "tesztelését (csak ne felejtse el a módosításokat alkalmazni)."

#~ msgid ""
#~ "If supported, this option allows you to setup the state of NumLock after "
#~ "Plasma startup.<p>You can configure NumLock to be turned on or off, or "
#~ "configure Plasma not to set NumLock state."
#~ msgstr ""
#~ "Ha a hardver támogatja, itt lehet beállítani a NumLock állapotát a Plasma "
#~ "indulásakor.<p>A NumLock ki- vagy bekapcsolható, illetve meghagyható az "
#~ "eredeti állapot."

#~ msgid "Leave unchan&ged"
#~ msgstr "Ne &változzon"

#~ msgid ""
#~ "If supported, this option allows you to set the delay after which a "
#~ "pressed key will start generating keycodes. The 'Repeat rate' option "
#~ "controls the frequency of these keycodes."
#~ msgstr ""
#~ "Ha a hardver támogatja ezt a lehetőséget, ennyi idő eltelte után kezdődik "
#~ "az ismétlés egy billentyű lenyomva tartásakor. Az ismétlődési sebesség "
#~ "érték adja meg a kibocsátott karakterek időközét."

#~ msgid ""
#~ "If supported, this option allows you to set the rate at which keycodes "
#~ "are generated while a key is pressed."
#~ msgstr ""
#~ "Ha a hardver támogatja, ilyen gyorsasággal keletkeznek új karakterek egy "
#~ "billentyű lenyomva tartásakor."

#~ msgid " repeats/s"
#~ msgstr " ismétlés/s"

#~ msgid " ms"
#~ msgstr " ms"

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "KDE billentyűzetbeállító modul"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "(C) Andriy Rysin, 2010."

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Billentyűzet</h1> Ebben a modulban állíthatja be a billentyűzet "
#~ "paramétereit és kiosztásait."

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "KDE billentyűzetkiosztás-váltó"

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Legfeljebb %1 kiosztás támogatott"
#~ msgstr[1] "Legfeljebb %1 kiosztás támogatott"

#~ msgid "Any language"
#~ msgstr "Bármely nyelv"

#~ msgid "Layout:"
#~ msgstr "Kiosztás:"

#~ msgid "Variant:"
#~ msgstr "Változat:"

#~ msgid "Limit selection by language:"
#~ msgstr "Kijelölés korlátozása nyelv szerint:"

#~ msgid "..."
#~ msgstr "…"

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 - %2"

#~ msgid "Layout Indicator"
#~ msgstr "Kiosztásjelző"

#~ msgid "Show layout indicator"
#~ msgstr "Kiosztásjelző megjelenítése"

#~ msgid "Show for single layout"
#~ msgstr "Megjelenítés az egyszeres kiosztáshoz"

#~ msgid "Show flag"
#~ msgstr "Zászló megjelenítése"

#~ msgid "Show label"
#~ msgstr "Címke megjelenítése"

#~ msgid "Show label on flag"
#~ msgstr "Címke megjelenítése a zászlón"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Billentyűzetkiosztás"

#~ msgid "Configure Layouts..."
#~ msgstr "Kiosztások beállítása…"

#~ msgid "Keyboard Repeat"
#~ msgstr "Billentyűismétlés"

#~ msgid "Turn o&ff"
#~ msgstr "&Ki"

#~ msgid "Configure..."
#~ msgstr "Beállítás…"

#~ msgid "Key Click"
#~ msgstr "Billentyű kattintás"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "Ha a hardver támogatja, beállítható, hogy billentyűleütéskor hangjelzést "
#~ "adjon a rendszer. Elsősorban akkor lehet hasznos, ha nem mechanikusak a "
#~ "billentyűk vagy alig hallható  a leütés. <p>A hangjelzés ereje "
#~ "beállítható a csúszkával vagy a számbeállító gomb nyilaival. 0% esetén "
#~ "nem lesz hangjelzés."

#, fuzzy
#~| msgid "Key click &volume:"
#~ msgid "&Key click volume:"
#~ msgstr "A hangjelzés &ereje:"

#~ msgid "No layout selected "
#~ msgstr "Nincs elrendezés kiválasztva "

#~ msgid "XKB extension failed to initialize"
#~ msgstr "Az XKB-bővítmény inicializálása meghiúsult"

#~ msgid "Backspace"
#~ msgstr "Backspace"

#~ msgctxt "Tab key"
#~ msgid "Tab"
#~ msgstr "Tab"

#~ msgid "Caps Lock"
#~ msgstr "Caps Lock"

#~ msgid "Enter"
#~ msgstr "Enter"

#~ msgid "Ctrl"
#~ msgstr "Ctrl"

#~ msgid "Alt"
#~ msgstr "Alt"

#~ msgid "AltGr"
#~ msgstr "AltGr"

#~ msgid "Esc"
#~ msgstr "Esc"

#~ msgctxt "Function key"
#~ msgid "F%1"
#~ msgstr "F%1"

#~ msgid "Shift"
#~ msgstr "Shift"

#~ msgid "No preview found"
#~ msgstr "Előnézet nem található"

#~ msgid "Close"
#~ msgstr "Bezárás"

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "Ha ez be van jelölve, billentyű lenyomva tartásakor a kibocsátott "
#~ "karakter ismétlődni fog. Ha például lenyomva tartja a Tab billentyűt, "
#~ "akkor az olyan, mintha egymás után megnyomná újból, azaz Tab karakterek "
#~ "keletkeznek egészen addig, amíg fel nem engedi a billentyűt."

#~ msgid "&Enable keyboard repeat"
#~ msgstr "Billentyű&ismétlés"
